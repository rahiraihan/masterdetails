﻿using System;
using System.Collections.Generic;

namespace MasterDetailsApi.Models
{
    public partial class Customer
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string ContactNumber { get; set; }
        public string PurchaseDate { get; set; }
        public string Description { get; set; }
    }
}
