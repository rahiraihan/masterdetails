﻿using System;
using System.Collections.Generic;

namespace MasterDetailsApi.Models
{
    public partial class OrderDetails
    {
        public int OrderId { get; set; }
        public string ItemName { get; set; }
        public string ItemQty { get; set; }
        public string ItemPrice { get; set; }
    }
}
